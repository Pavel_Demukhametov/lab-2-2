import $ from 'jquery';
window.bootstrap = require('bootstrap/dist/js/bootstrap.js');
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
});


document.getElementById("toastbtn").onclick = function() {
  var myAlert =document.getElementById('toastNotice');//select id of toast
    var bsAlert = new bootstrap.Toast(myAlert);//inizialize it
    bsAlert.show();//show it
};


const fillModal = (modal, source)=>{
  modal.find(".modal-title").html(source.find(".card-title").html())
  modal.find(".modal-body").html(source.find(".card-text").html())
  modal.attr('current-item', source.attr('id'));
};

$(document).on('click', '.card', function (e) {
  const modal = $(document).find("#modal");
  fillModal(modal, $(this));
});

$(document).on('keydown', function(e) {
  const modal = $(document).find("#modal");
  const cardsList = $(document).find('.card').toArray().map((item)=>{
    return $(item).attr('id')
  });
  const current = cardsList.findIndex(element => element == modal.attr("current-item"));

  if(current == -1){
    throw new Error("invalid card index");
  }

  if(e.which == 37){ // Left Arrow Key
    const prevSlide = current > 0 ? cardsList[current-1] : cardsList[cardsList.length - 1];
    fillModal(modal, $(document).find(`#${prevSlide}`));
  } else if (e.which == 39){ // Right Arrow Key
    const nextSlide = current < cardsList.length-1 ? cardsList[current+1] : cardsList[0];
    fillModal(modal, $(document).find(`#${nextSlide}`));
  }
});

$(document).on('click', '#modal .slide-btn', function(){
  const modal = $(document).find("#modal");
  const cardsList = $(document).find('.card').toArray().map((item)=>{
    return $(item).attr('id')
  });
  const current = cardsList.findIndex(element => element == modal.attr("current-item"));

  if(current == -1){
    throw new Error("invalid card index");
  }
  
  if($(this).hasClass("btn-prev")){
    const prevSlide = current > 0 ? cardsList[current-1] : cardsList[cardsList.length - 1];
    fillModal(modal, $(document).find(`#${prevSlide}`));
  } else if($(this).hasClass("btn-next")){
    const nextSlide = current < cardsList.length-1 ? cardsList[current+1] : cardsList[0];
    fillModal(modal, $(document).find(`#${nextSlide}`));
  }
});